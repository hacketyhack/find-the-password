# lvl1 Keygen Crackme - OS Linux-Ubuntu

From [crackmes.one](https://crackmes.one/crackme/5eae2d6633c5d47611746500).

## Setup: 
- Can confirm that the desired file `easyAF` is an ELF by typing `file easyAF`.
- Next we want to produce the assembly and save it into a file that we can analyze
and take notes in via the command line: `objdump -dC | gvim -`.
- Save the file as `easyAF.s`.

## Assembly:
See [easyAF.s](easyAF.s#L164)

- Upon inspecting the assembly, we can tell that this program actively uses a variety of c++ functions.
- After an initial scan of the file, we will start looking in detail at the `<main>` label. 

### `main`:
- The first three lines we can see the function prepares the stack.
- Then the program creates room on the stack for local variables on [line 172](easyAF.s#L172).
- A stack canary is then created ([line 173](easyAF.s#L173)) and placed in `-0x18(%rbp)`. The last
4 bytes are cleared, and then erased.
- We can see that the program makes two calls to `std::basic_string()` which is a string constructor. These appear to be
interesting variables to inspect.
- On the command line we can use `strings easyAF` to dump all printable strings from the binary. This produced a 
jumble of strings, but the four readable lines were: `pass`, `Enter the password:`, `Welldone!`, and `Nope`. 
-  We see one of the strings used on [line 198](easyAF.s#L198) when the program prints the prompt, requesting user input: `Enter the password:`.
- The string `Nope` matches the prompt when we enter the wrong password. 
- That leaves two strings, `pass`, and `Welldone!` as possible fixed string password.
- `pass` falls into the category of most commonly used passwords, so we'll try that one first.
- `pass` is the fixed string password and `Welldone!` we see is the prompt printed on success.

## Keygen:
See [easyAF_script.sh](easyAF_script.sh) 

Being that the password is a fixed string, this requires a very simple bash script.
The first line indicates that it is a bash script.
The second line is the instruction to pass our string `pass` into the program that is too be run `./easyAF`. 
Thus we have successfully created a script to run and win the keygen every time.

```console 
shazari@wonderland:~/code/crackmes/find-the-password$ ./easyAF_script.sh
Enter the password: Welldone!
```

## Reconstruction:
As a further exercise, we chose to reconstruct the original crackme from the assembly that we have just analyzed. See [easyAF_reconstruct.cpp](easyAF_reconstruct.cpp)

- This program uses the following headers: `<string>` for string functionality, `<iostream>` for input/output to screen, 
and `<cstdlib>` for call to `exit()`. 
- As we saw in the assembly, the program first creates an empty string, then assigns the fixed password to it.
- An empty string is created, which will be filled by calling `std::cin`, filling it with the user input. 
- The program prints the first prompt `Enter the password: ` 
- Saves user input into the empty string mentioned above.
- Goes into an `if` statement checking the password equality to the user input.
- If true it prints the success message. Then by-passes the following `else if` case and returns 0.
- Otherwise the program continues to the next `else if` statement for the false case. If this holds, it will print the failure message
and exit with 0.

## Analysis:
- This program uses stack canaries to ensure that the stack remains intack and buffer overflow did not occur.
- The program relies on the autmatic clean up of c++ to deconstruct and free the memory allocated to the strings when the program ends.
This avoids memory leaks.
- Oddly, the program uses a `if` statement followed by an `else if`, rather than the standard and more simplistic `else`. The `else if` statement is a redundant inverse check of the original `if`, thus not actually necessary. To maintain efficency the use of just an
`else` would be preferred.
- Personally, we would have liked to see a new line included after the success and failure message but this is a stylistic choice.

