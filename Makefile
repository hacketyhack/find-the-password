easyAF_reconstruct: easyAF_reconstruct.cpp 
	g++ -Wall -Wextra -Wpedantic -Wshadow -g -o $@ $^

clean: 
	rm -f easyAF_reconstruct

.PHONY: clean
